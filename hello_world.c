#include <stdio.h>

#define VERSION "{{VERSION}}"

int main(int argc, char** argv) {
    printf("Hello world, I am version %s\n", VERSION);
}
