# release-links

This project uses tags and a branch named `latest` to re-expose the latest
tagged artifacts as `latest` artifacts.

## File Links

### Latest

* [gitlab-hello-world_latest_amd64.deb](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/latest/raw/packages/gitlab-hello-world_latest_amd64.deb?job=latest-build)
* [gitlab_hello_world-latest-1.x86_64.rpm](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/latest/raw/packages/gitlab_hello_world-latest-1.x86_64.rpm?job=latest-build)
* [gitlab-hello-world.tar](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/latest/raw/packages/gitlab_hello_world.tar?job=latest-build)
* [gitlab-hello-world.zip](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/latest/raw/packages/gitlab_hello_world.zip?job=latest-build)

### v4.0.2

* [gitlab-hello-world_4.0.2_amd64.deb](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.2/raw/packages/gitlab-hello-world_4.0.2_amd64.deb?job=version-build)
* [gitlab_hello_world-4.0.2-1.x86_64.rpm](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.2/raw/packages/gitlab_hello_world-4.0.2-1.x86_64.rpm?job=version-build)
* [gitlab-hello-world.tar](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.2/raw/packages/gitlab_hello_world.tar?job=version-build)
* [gitlab-hello-world.zip](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.2/raw/packages/gitlab_hello_world.zip?job=version-build)

### v4.0.1

* [gitlab-hello-world_4.0.1_amd64.deb](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.1/raw/packages/gitlab-hello-world_4.0.1_amd64.deb?job=version-build)
* [gitlab_hello_world-4.0.1-1.x86_64.rpm](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.1/raw/packages/gitlab_hello_world-4.0.1-1.x86_64.rpm?job=version-build)
* [gitlab-hello-world.tar](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.1/raw/packages/gitlab_hello_world.tar?job=version-build)
* [gitlab-hello-world.zip](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.1/raw/packages/gitlab_hello_world.zip?job=version-build)

### v4.0.0

* [gitlab-hello-world_4.0.0_amd64.deb](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.0/raw/packages/gitlab-hello-world_4.0.0_amd64.deb?job=version-build)
* [gitlab_hello_world-4.0.0-1.x86_64.rpm](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.0/raw/packages/gitlab_hello_world-4.0.0-1.x86_64.rpm?job=version-build)
* [gitlab-hello-world.tar](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.0/raw/packages/gitlab_hello_world.tar?job=version-build)
* [gitlab-hello-world.zip](https://gitlab.com/d0c-s4vage-tests/release-links/-/jobs/artifacts/v4.0.0/raw/packages/gitlab_hello_world.zip?job=version-build)
